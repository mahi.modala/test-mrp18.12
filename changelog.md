# changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v1.0.1] - 2021-10-01

## mrp version
`apache-ofbiz-18.12.01`

### Integrate postgres in mrp
- do the following changes in MRP to integrate postgres.
      - goto /ofbiz/framework/entity/config/entityengine.xml
      - change 
        `<delegator name="default" entity-model-reader="main" entity-group-reader="main" entity-eca-reader="main"
         distributed-cache-clear-enabled="false">
        <group-map group-name="org.apache.ofbiz" datasource-name="localderby"/>
        <group-map group-name="org.apache.ofbiz.olap" datasource-name="localderbyolap"/>
        <group-map group-name="org.apache.ofbiz.tenant" datasource-name="localderbytenant"/>
        </delegator>
        
        <delegator name="default-no-eca" entity-model-reader="main" entity-group-reader="main" entity-eca-reader="main" entity-eca-enabled="false" distributed-cache-clear-enabled="false">
        <group-map group-name="org.apache.ofbiz" datasource-name="localderby"/>
        <group-map group-name="org.apache.ofbiz.olap" datasource-name="localderbyolap"/>
        <group-map group-name="org.apache.ofbiz.tenant" datasource-name="localderbytenant"/>
        </delegator>
        
        <delegator name="test" entity-model-reader="main" entity-group-reader="main" entity-eca-reader="main">
        <group-map group-name="org.apache.ofbiz" datasource-name="localderby"/>
        <group-map group-name="org.apache.ofbiz.olap" datasource-name="localderbyolap"/>
        <group-map group-name="org.apache.ofbiz.tenant" datasource-name="localderbytenant"/>
        </delegator>`

        To 

        `<delegator name="default" entity-model-reader="main" entity-group-reader="main" entity-eca-reader="main" distributed-cache-clear-enabled="false">
        <group-map group-name="org.apache.ofbiz" datasource-name="localpostgres"/>
        <group-map group-name="org.apache.ofbiz.olap" datasource-name="localpostolap"/>
        <group-map group-name="org.apache.ofbiz.tenant" datasource-name="localposttenant"/>
        </delegator>
        
        <delegator name="default-no-eca" entity-model-reader="main" entity-group-reader="main" entity-eca-reader="main" entity-eca-enabled="false" distributed-cache-clear-enabled="false">
        <group-map group-name="org.apache.ofbiz" datasource-name="localpostgres"/>
        <group-map group-name="org.apache.ofbiz.olap" datasource-name="localpostolap"/>
        <group-map group-name="org.apache.ofbiz.tenant" datasource-name="localposttenant"/>
        </delegator>

        <delegator name="test" entity-model-reader="main" entity-group-reader="main" entity-eca-reader="main">
        <group-map group-name="org.apache.ofbiz" datasource-name="localpostgres"/>
        <group-map group-name="org.apache.ofbiz.olap" datasource-name="localpostolap"/>
        <group-map group-name="org.apache.ofbiz.tenant" datasource-name="localposttenant"/>
        </delegator>`

      - change datasource named `localpostgres` , `localpostolap` and `localposttenant` in the inline-jdbc section.
        `<datasource name="localpostgres"
            helper-class="org.apache.ofbiz.entity.datasource.GenericHelperDAO"
            schema-name="public"
            field-type-name="postgres"
            check-on-start="true"
            add-missing-on-start="true"
            use-fk-initially-deferred="false"
            alias-view-columns="false"
            join-style="ansi"
            use-binary-type-for-blob="true"
            use-order-by-nulls="true"
            result-fetch-size="50">
            <read-data reader-name="tenant"/>
            <read-data reader-name="seed"/>
            <read-data reader-name="seed-initial"/>
            <read-data reader-name="demo"/>
            <read-data reader-name="ext"/>
            <read-data reader-name="ext-test"/>
            <read-data reader-name="ext-demo"/>
            <inline-jdbc
                jdbc-driver="org.postgresql.Driver"
                jdbc-uri="jdbc:postgresql://127.0.0.1:5432/ofbiz"
                jdbc-username="postgres"
                jdbc-password="postgres"
                isolation-level="ReadCommitted"
                pool-minsize="2"
                pool-maxsize="250"
                time-between-eviction-runs-millis="600000"/>
         </datasource>`

        `<datasource name="localpostolap"
             helper-class="org.apache.ofbiz.entity.datasource.GenericHelperDAO"
             schema-name="public"
             field-type-name="postgres"
             check-on-start="true"
             add-missing-on-start="true"
             use-fk-initially-deferred="false"
             alias-view-columns="false"
             join-style="ansi"
             result-fetch-size="50"
             use-binary-type-for-blob="true"
             use-order-by-nulls="true">
             <read-data reader-name="tenant"/>
             <read-data reader-name="seed"/>
             <read-data reader-name="seed-initial"/>
             <read-data reader-name="demo"/>
             <read-data reader-name="ext"/>
             <read-data reader-name="ext-test"/>
             <read-data reader-name="ext-demo"/>
             <inline-jdbc
                 jdbc-driver="org.postgresql.Driver"
                 jdbc-uri="jdbc:postgresql://127.0.0.1:5432/ofbizolap"
                 jdbc-username="postgres"
                 jdbc-password="postgres"
                 isolation-level="ReadCommitted"
                 pool-minsize="2"
                 pool-maxsize="250"
                 time-between-eviction-runs-millis="600000"/>
         </datasource>`

         `<datasource name="localposttenant"
             helper-class="org.apache.ofbiz.entity.datasource.GenericHelperDAO"
             schema-name="public"
             field-type-name="postgres"
             check-on-start="true"
             add-missing-on-start="true"
             use-fk-initially-deferred="false"
             alias-view-columns="false"
             join-style="ansi"
             result-fetch-size="50"
             use-binary-type-for-blob="true"
             use-order-by-nulls="true">
             <read-data reader-name="tenant"/>
             <read-data reader-name="seed"/>
             <read-data reader-name="seed-initial"/>
             <read-data reader-name="demo"/>
             <read-data reader-name="ext"/>
             <read-data reader-name="ext-test"/>
             <read-data reader-name="ext-demo"/>
             <inline-jdbc
                jdbc-driver="org.postgresql.Driver"
                jdbc-uri="jdbc:postgresql://127.0.0.1:5432/ofbiztenant"
                jdbc-username="postgres"
                jdbc-password="postgres"
                isolation-level="ReadCommitted"
                pool-minsize="2"
                pool-maxsize="250"
                time-between-eviction-runs-millis="600000"/>
         </datasource>`
      - `jdbc-username` and `jdbc-password` one can choose and same has to be given in the `localpostgres`, `localpostolap`,
        `localposttenant`   datasource.

      - goto /ofbiz/build.gradle
           - add this line depending on the version used:line no: 197

             `compile 'org.postgresql:postgresql:42.1.1'`
             or
             `implementation 'org.postgresql:postgresql:42.1.1'`

### Implementing multitenancy in MRP
- to implement multitenancy in MRP , goto /ofbiz/framework/common/config/general.properties
    - change `multitenant=Y`.

### creating a new tenant
- Create DB user and DB for the tenant.
- tenants are identified in OFBiz using tenantID lets say we want to create a new tenant with id: tenant001 , we should be creating 2
  databases with names ofbiz_tenant001 and ofbizolap_tenant001.
         $ sudo -s
         [sudo] password for <user>: <enter pass>
         # su - postgres
         $ createuser --createdb --login -P ofb_tenant001            
         Enter password for new role:  ofbiz@tenant
         Enter it again: ofbiz@tenant
         $ createdb -O ofb_tenant001  ofbiz_tenant001
         $ createdb -O ofb_tenant001  ofbizolap_tenant001
- run command to create a new tenant.
  `/gradlew createTenant -PtenantId=tenant001   -PtenantName="My Tenant 001" -PdomainName=tenant001.example.com -PtenantReaders=seed,seed-initial,ext -PdbPlatform=P -PdbIp=127.0.0.1 -PdbUser=ofb_tenant001 -PdbPassword=ofbiz@tenant`
- this command will creates a tenant with id `tenant001` , essentially it puts the meta data in the tables in the tenants datasource namely
  `ofbizdb_tenant`
- review the data in those tables by running these commands in postgres.
  `psql -h localhost -Uofbizuser_tenant ofbizdb_tenant`   
          ofbizdb_tenant=> \dt 
                           List of relations 
          Schema |           Name            | Type  |      Owner        
         --------+---------------------------+-------+------------------ 
          public | component                 | table | ofbizuser_tenant 
          public | tenant                    | table | ofbizuser_tenant 
          public | tenant_component          | table | ofbizuser_tenant 
          public | tenant_data_source        | table | ofbizuser_tenant 
          public | tenant_domain_name        | table | ofbizuser_tenant 
          public | tenant_key_encrypting_key | table | ofbizuser_tenant 
          (6 rows) 
 
          ofbizdb_tenant=> \x 
          Expanded display is on. 
          ofbizdb_tenant=> select * from tenant; 
          -[ RECORD 1 ]---------+------------------------------ 
          tenant_id             | tenant001 
          tenant_name           | My Tenant 001 
          initial_path          |  
          disabled              |  
          last_updated_stamp    | 2018-03-11 23:30:51.567+05:30 
          last_updated_tx_stamp | 2018-03-11 23:30:51.561+05:30 
          created_stamp         | 2018-03-11 23:30:51.567+05:30 
          created_tx_stamp      | 2018-03-11 23:30:51.561+05:30 
 
          ofbizdb_tenant=> select * from tenant_data_source; 
          -[ RECORD 1 ]---------+------------------------------------------------ 
          tenant_id             | tenant001 
          entity_group_name     | org.apache.ofbiz 
          jdbc_uri              | jdbc:postgresql://127.0.0.1/ofbiz_tenant001 
          jdbc_username         | ofb_tenant001  
          jdbc_password         | ofbiz@tenant 
          last_updated_stamp    | 2018-03-11 23:30:51.575+05:30 
          last_updated_tx_stamp | 2018-03-11 23:30:51.561+05:30 
          created_stamp         | 2018-03-11 23:30:51.575+05:30 
          created_tx_stamp      | 2018-03-11 23:30:51.561+05:30 
          -[ RECORD 2 ]---------+------------------------------------------------ 
          tenant_id             | tenant001 
          entity_group_name     | org.apache.ofbiz.olap 
          jdbc_uri              | jdbc:postgresql://127.0.0.1/ofbizolap_tenant001 
          jdbc_username         | ofb_tenant001 
          jdbc_password         | ofbiz@tenant 
          last_updated_stamp    | 2018-03-11 23:30:51.577+05:30 
          last_updated_tx_stamp | 2018-03-11 23:30:51.561+05:30 
          created_stamp         | 2018-03-11 23:30:51.577+05:30 
          created_tx_stamp      | 2018-03-11 23:30:51.561+05:30 
 
          ofbizdb_tenant=> select * from  tenant_domain_name; 
          -[ RECORD 1 ]---------+------------------------------ 
          tenant_id             | tenant001 
          domain_name           | tenant001.example.com 
          last_updated_stamp    | 2018-03-11 23:30:51.574+05:30 
          last_updated_tx_stamp | 2018-03-11 23:30:51.561+05:30 
          created_stamp         | 2018-03-11 23:30:51.574+05:30 
          created_tx_stamp      | 2018-03-11 23:30:51.561+05:30

### visit ofbiz through browser
- `https://localhost:8443/webtools/control/main`
  `username:admin`
  `password:ofbiz`
- if multitenancy is enabled, then 
    
  `https://localhost:8443/webtools/control/main`
  `username:tenantId-admin`
  `password:ofbiz`
  `tenantId:tenantId`

- example:
  `username:tenant001-admin`
  `password:ofbiz`
  `tenantId:tenant001`

### Exporting csv file from mrp
- to export any table data into csv file from mrp, navigate to root directory of mrp ,open `postgres` and run command
  `\copy table_name to 'destinationLocation\filename.csv' csv header;`

### Importing csv file into mrp
- login to mrp, goto webtools->import\export, convert the csv file to xml and put the xml code into `entity-engine-xml` and click on import
  button.

       

## [v1.0.0] - 2021-09-23
 

### Changed
- X-MRP colors to xerus color code [43-47-54] [41-39-65] in Tomahawk Theme => changed background,screen colors and module colors.
- change `privileged administrator` to `Admin`.
- change ofbiz favicon to xerus favicon with transparent background.
- change `party` module to `users`.
- change `manufacturing` module to `production`.
- change `Facility` module to `Inventory`.



### Removed
- `ofbiz logo` from the header.
- remove navigation icons from navigation bar.
- remove navigation text from navigation bar => `visual themes`, `language`.
- remove few modules from mrp that are not needed => `work effort`, `content`, `HR`, `marketing`, `portal`, `project`, `SFA`, `scrum`    `webPOS`, `ebay`.
- remove sub modules and screens from all the modules to make it simple as it was crowded.


### Added
- `X-MRP logo` with xerus color code.
- add screen to the home page which contains necessery modules.
- add `www.xerussystems.com` to footer, by clicking on it,it should land on home page.
- add social media icons to footer.
- add a `HOME page` for xerus with necessery modules and add href to the modules to reach out to the respected module pages.
- add plugin named xerus with two screens
       - add new xerus employee
       - list all the employees


### Fixed
- fix `X-MRP logo` as it was squished.
- on clicking `mrp logo` in any page, it should land on the `HOME page`.
