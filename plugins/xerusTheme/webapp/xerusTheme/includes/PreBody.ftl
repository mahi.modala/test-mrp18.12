<html>
  <head>
    <title>${layoutSettings.companyName}</title>
    <#--  <meta name="viewport" content="width=device-width, user-scalable=no"/>
    <#if webSiteFaviconContent?has_content>
    <link href="images/favicon.png" rel="icon" type="image/png">
    </#if>  -->


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>${layoutSettings.companyName}: <#if (titleProperty)?has_content>${uiLabelMap[titleProperty]}<#else>${title!}</#if></title>
    <#if layoutSettings.shortcutIcon?has_content>
      <#assign shortcutIcon = layoutSettings.shortcutIcon/>
    <#elseif layoutSettings.VT_SHORTCUT_ICON?has_content>
      <#assign shortcutIcon = layoutSettings.VT_SHORTCUT_ICON/>
    </#if>
    <#if shortcutIcon?has_content>
        <link rel="shortcut icon" href="<@ofbizContentUrl>${StringUtil.wrapString(shortcutIcon)+".ico"}</@ofbizContentUrl>" type="image/x-icon">
        <link rel="icon" href="<@ofbizContentUrl>${StringUtil.wrapString(shortcutIcon)+".png"}</@ofbizContentUrl>" type="image/png">
        <link rel="icon" sizes="32x32" href="<@ofbizContentUrl>${StringUtil.wrapString(shortcutIcon)+"-32.png"}</@ofbizContentUrl>" type="image/png">
        <link rel="icon" sizes="64x64" href="<@ofbizContentUrl>${StringUtil.wrapString(shortcutIcon)+"-64.png"}</@ofbizContentUrl>" type="image/png">
        <link rel="icon" sizes="96x96" href="<@ofbizContentUrl>${StringUtil.wrapString(shortcutIcon)+"-96.png"}</@ofbizContentUrl>" type="image/png">
    </#if>
    
    <#--  ------------------------------------------------------------------------  -->
    <#list layoutSettings.styleSheets as styleSheet>
      <link rel="stylesheet" href="${StringUtil.wrapString(styleSheet)}" type="text/css"/>
    </#list>
    <#list layoutSettings.javaScripts as javaScript>
      <script type="text/javascript" src="${StringUtil.wrapString(javaScript)}"></script>
    </#list>
    
  </head>
  <body>
    <div id="home-masthead">
           <ul>  
              <#--  <li id="home-org-logo-area">
                    <a href="https://localhost:8443/home/control/main?externalLoginKey=ELc9d17490-8b53-4c0f-a462-8ae6763c207e">
                       <img alt="${layoutSettings.companyName}" 
                         src="#">
                         <img alt="${layoutSettings.companyName}" src="/images/ofbiz_logo.png">
                    </a>
              </li>  -->
                <li id="home-logo-area">
                    <a href="https://localhost:8443/xerusTheme/control/main"
                       title="${layoutSettings.companyName!}">
                       <img alt="OFBiz: xerusTheme" src="/images/ofbiz_logo.png">
                    </a>
              </li>
           </ul>
        <#--  <br class="clear" />  -->
    </div>
    <hr>
    
                                              <#--  home menu-bar logout  -->
      <div class="container-menus" id="home-menu-container">      
          <ul id="home-page-title" class="breadcrumb">
            <li id="home-list">
                <a id="home-main" href="https://localhost:8443/xerusTheme/control/main">Home</a>
                <#--  <a href="<@ofbizUrl>main</@ofbizUrl>">Main</a>  -->
            </li>   
            <li class="home-pull-right">
                <a id="home-logout" href="<@ofbizUrl>logout</@ofbizUrl>" title="${uiLabelMap.CommonLogout}">Logout</a>
            </li>
          </ul>
      </div>

                                             <#--  home page grey container content  -->
    <div class="home-main-container-body">
        <div class="container"> 

            <div class="box-container">
               <a  href="https://localhost:8443/assetmaint/control/main?externalLoginKey=ELfccd14d0-0b7b-4ded-844d-d8a9b950187c" class="box">
                 
                  <div id="assetm"></div>
                    <div class="div-head">
                        <h1 id="assethead">Asset Maintenance</h1>
                        <p id="assetp">It enables organisations to maintain a register of all kinds of assets</p>
                    </div>
                    <#--  <a href="https://localhost:8443/assetmaint/control/main?externalLoginKey=ELfccd14d0-0b7b-4ded-844d-d8a9b950187c" class="btn"></a>  -->
                </a>

                <a href="https://localhost:8443/catalog/control/main?externalLoginKey=ELfccd14d0-0b7b-4ded-844d-d8a9b950187c"class="box">
                  <div id="prodcat"></div>
                    <div class="div-head">
                  
                        <h1 id="prodcathead">Product Catalog</h1>
                        <p id="prodcatp">It provides various features like catalog and product management,promotion and price rules management</p>
                    </div>
                </a>

                <a href="https://localhost:8443/xerus/control/ProductionDashboardFtl" class="box">
                  <div id="productionm"></div>
                    <div class="div-head">
                       
                       <h1 id="productionhead">Production</h1>
                       <p id="productionp">It provides manufacturing features like Bill of materials,production planning and production costing</p>
                      </div>
                </a>
    
                <a href="https://localhost:8443/ordermgr/control/main?externalLoginKey=ELe412b3d4-a7a5-42a8-bc8c-9be077c664b9" class="box">
                  <div id="orderm"></div>
                    <div class="div-head">
                      
                      <h1 id="orderhead">Product Order</h1>
                      <p id="orderp">It gives complete control and design of order information of customers</p>
                    </div>
                </a>

                <a href="https://localhost:8443/partymgr/control/main?externalLoginKey=EL154c28ab-c21d-4004-8ab4-cc18d49bbe65" class="box">
                  <div id="usersm"></div>
                    <div class="div-head">
                      
                      <h1 id="usershead">Users</h1>
                      <p id="usersp">It provides a facility to create the entities(companies, people, groups)that we deal with in the course of doing business</p>
                    </div>
                </a>

                <a href="https://localhost:8443/facility/control/main?externalLoginKey=ELfccd14d0-0b7b-4ded-844d-d8a9b950187c" class="box">
                   <div id="invm"></div>
                      <div class="div-head">
                        
                         <h1 id="invhead">Warehouse Management</h1>
                         <p id="invp">It manages the storage and movement of the material in a warehouse, from receiving, picking, and shipping</p>
                      </div>
                </a>
                    
            </div>   
        </div>

    </div>

                                                     <#--  home page footer  -->

    <div id="footer">
  <#--  <div class="xerus-footer">  -->
      <ul>
          <li id="footertext">
              ${uiLabelMap.CommonCopyright} (c) 2019-${nowTimestamp?string("yyyy")} xerussystems - <a href="http://localhost:8443/xerusTheme/control/main?externalLoginKey=EL94fb78d6-a18c-48cb-954b-9fc115de9fab" target="_blank">www.xerussystems.com</a>
          <br/>
              ${uiLabelMap.CommonPoweredBy} <a href="https://localhost:8443/xerusTheme/control/main?externalLoginKey=EL94fb78d6-a18c-48cb-954b-9fc115de9fab" target="_blank">Xerus Systems LLC</a>  All Rights Reserved
    
          </li>
    <#--  <li class="opposed">
    
    </li>  -->
      </ul>
    </div>                                                 