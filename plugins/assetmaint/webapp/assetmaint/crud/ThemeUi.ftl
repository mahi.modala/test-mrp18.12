<div class="screenlet-body">
    <#if !userLogin?has_content>
      <div>${uiLabelMap.WebtoolsForSomethingInteresting}.</div>
      <br />
      <div>${uiLabelMap.WebtoolsNoteAntRunInstall}</div>
      <br />
      <div><a href="<@ofbizUrl>checkLogin</@ofbizUrl>">${uiLabelMap.CommonLogin}</a></div>
    </#if>
    <#if userLogin?has_content>
    <div class="nav-sidebar" id="navigation-sidebar">
      <ul class="webToolList" id="home">
         <li><a href="/manufacturing/control/main?externalLoginKey=ELf8650276-c0c1-4952-9936-fd87e090beff"><h2 id="production">${uiLabelMap.XerusThemeProduction}</h2></a></li>
         <li><a href="/facility/control/main?externalLoginKey=ELfccd14d0-0b7b-4ded-844d-d8a9b950187c"><h2 id="inv">${uiLabelMap.XerusThemeInventoryManagement}</h2></a></li>
         <li><a href="/assetmaint/control/main?externalLoginKey=ELfccd14d0-0b7b-4ded-844d-d8a9b950187c"><h2 id="asset">${uiLabelMap.XerusThemeAssetMaintenance}</h2></a></li>
         <li><a href="/ordermgr/control/main?externalLoginKey=ELe412b3d4-a7a5-42a8-bc8c-9be077c664b9"><h2 id="order">${uiLabelMap.XerusThemeOrder}</h2></a></li>
         <li><a href="/partymgr/control/main?externalLoginKey=EL154c28ab-c21d-4004-8ab4-cc18d49bbe65"><h2 id="users">${uiLabelMap.XerusThemeUsers}</h2></a></li>

         <li><a href="/catalog/control/main?externalLoginKey=ELfccd14d0-0b7b-4ded-844d-d8a9b950187c"><h2 id="prodcatalog">${uiLabelMap.XerusThemeProductCatalog}</h2></a></li>
        
     </ul>
     </div>
    </#if>  
</div>