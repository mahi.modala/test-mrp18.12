<div class="screenlet-body">

      <div class="dashboard-container-body">
        <div class="dashboard-container"> 

            <div class="dashboard-box-container">

                <div class="dashboard-box">
                    <div class="content">
                         <form action="/" class="form">
                            <label class="text" for="productname">Name of the Product:</label>
                            <input type="text" id="productname" name="productname">
                         </form>
                    </div>
                        <h2 class="dashboard-container-box-heading" id="dashboard-heading">Product Name and ID</h2>
                      
                </div>

                <div class="dashboard-box">
                    <div class="content">
                          <form action="/" class="form">
                            <label class="text" for="productionrunname">Production Run Name:</label>
                            <input type="text" id="productionrun-name" name="productionrunname">
                            <br><br>
                            <label class="text" for="quantitylastproduced">Quantity Last Produced:</label>
                            <input type="text" id="quantitylastproduced" name="quantitylastproduced">
                         </form>
                    </div>                  
                        <h2 class="dashboard-container-box-heading" id="dashboard-heading">Quantity produced in Last Run</h2>
                   
                </div>

                <div class="dashboard-box">                
                    <div class="content">
                          <form action="/" class="form">
                            <label class="text" for="quantityfallingshort">Quantity Falling Short:</label>
                            <input type="text" id="quantity-falling-short" name="quantityfallingshort">
                          </form>
                    </div>         
                       <h2 class="dashboard-container-box-heading" id="dashboard-heading">ShortFall</h2>
                     
                </div>
    
                <div class="dashboard-box">
                        <div class="content">
                           <form action="/" class="form">
                            <label class="text" for="innexthours">In Next 24 Hrs:</label>
                            <input type="text" id="innexthours" name="innexthours">
                            <br><br>
                            <label class="text" for="nextweek">Next Week:</label>
                            <input type="text" id="nextweek" name="nextweek">
                         </form>
                        </div>                      
                       <h2 class="dashboard-container-box-heading" id="dashboard-heading">Quantity to be produced</h2>
                      
                </div>

                <div class="dashboard-box">
                       <div class="content">
                           <form action="/" class="form">
                            <label class="text" for="innexthours">In Next 24 Hrs:</label>
                            <input type="text" id="innexthours" name="innexthours">
                            <br><br>
                            <label class="text" for="nextweek">Next Week:</label>
                            <input type="text" id="nextweek" name="nextweek">
                         </form>
                       </div>             
                       <h2 class="dashboard-container-box-heading" id="dashboard-heading">Production Run capacity</h2>
                      
                </div>

                <div class="dashboard-box">
                       <div class="content">
                           <form action="/" class="form">
                            <label class="text" for="days">Days:</label>
                            <input type="text" id="days" name="days">
                            <label class="text" for="time">Time:</label>
                            <input type="text" id="time" name="time">
                         </form>
                       </div>                 
                       <h2 class="dashboard-container-box-heading" id="dashboard-heading">Expected Time for product manufacturing</h2>
                      
                </div>
                    
            </div>   
        </div>

      <a class="btn" href="#">
         <input  id="continue-button" class="btn" type="submit" value="Continue">
         <input  id="go-back-button" class="btn" type="submit" value="Go Back  <<">
      </a>
         
         
      
    </div>
      
    
</div>