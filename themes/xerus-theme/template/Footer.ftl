<#--
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
-->

<#assign nowTimestamp = Static["org.apache.ofbiz.base.util.UtilDateTime"].nowTimestamp()>

<#--  <div id="footer">
  <div class="xerus-footer">
  <ul>
    <li id="footertext">
      ${uiLabelMap.CommonCopyright} (c) 2019-${nowTimestamp?string("yyyy")} xerussystems - <a href="http://localhost:8443/xerusTheme/control/main?externalLoginKey=EL94fb78d6-a18c-48cb-954b-9fc115de9fab" target="_blank">www.xerussystems.com</a>
      <br/>
      ${uiLabelMap.CommonPoweredBy} <a href="https://localhost:8443/xerusTheme/control/main?externalLoginKey=EL94fb78d6-a18c-48cb-954b-9fc115de9fab" target="_blank">Xerus Systems LLC</a>  All Rights Reserved
    
    </li> 
  </ul>
</div>  -->



<#--  </div>  -->
<#--  -----------------------------------------  -->
<footer id="home-footer">
    <ul>
       <li>${uiLabelMap.CommonCopyright} (c) 2019-${nowTimestamp?string("yyyy")} xerussystems - <a href="http://localhost:8443/xerusTheme/control/main?externalLoginKey=EL94fb78d6-a18c-48cb-954b-9fc115de9fab" target="_blank">www.xerussystems.com</a>
          <br/>
          ${uiLabelMap.CommonPoweredBy} <a href="https://localhost:8443/xerusTheme/control/main?externalLoginKey=EL94fb78d6-a18c-48cb-954b-9fc115de9fab" target="_blank">Xerus Systems LLC</a>  All Rights Reserved
       </li>
    </ul>   

</footer>


<#if layoutSettings.VT_FTR_JAVASCRIPT?has_content>
  <#list layoutSettings.VT_FTR_JAVASCRIPT as javaScript>
    <script src="<@ofbizContentUrl>${StringUtil.wrapString(javaScript)}</@ofbizContentUrl>" type="application/javascript"></script>
  </#list>
</#if> 



</div>
</body>
</html>
