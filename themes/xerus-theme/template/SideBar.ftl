<div class="nav">
      <ul>
          <li>
              <a href="/manufacturing/control/main?externalLoginKey=ELf8650276-c0c1-4952-9936-fd87e090beff">
                 <span class="icon"><i class='fas fa-globe' style='font-size:24px'></i></span>
                 <span class="title">Production</span>
                <#--  Production  -->
              </a>
          </li>
          <li>
              <a href="/facility/control/main?externalLoginKey=ELfccd14d0-0b7b-4ded-844d-d8a9b950187c">
                 <span class="icon"><i style='font-size:24px' class='fas'>&#xf66f;</i></span>
                 <span class="title">Warehouse Management</span>
                <#--  Inventory Management  -->
              </a>
          </li>
          <li>
              <a href="/assetmaint/control/main?externalLoginKey=ELfccd14d0-0b7b-4ded-844d-d8a9b950187c">
              <#--  Asset maintenance  -->
                 <span class="icon"><i style='font-size:24px' class='fas'>&#xf013;</i></span>
                 <span class="title">Asset Maintenance</span>
              </a>
          </li>
          <li>
              <a href="/ordermgr/control/main?externalLoginKey=ELe412b3d4-a7a5-42a8-bc8c-9be077c664b9">
              <#--  Order  -->
                 <span class="icon"><i style='font-size:24px' class='fas'>&#xf0d1;</i></span>
                 <span class="title">Product Order</span>
              </a>
          </li>
          <li>
              <a href="/partymgr/control/main?externalLoginKey=EL154c28ab-c21d-4004-8ab4-cc18d49bbe65">
              <#--  Users  -->
                 <span class="icon"><i style='font-size:24px' class='fas'>&#xf0c0;</i></span>
                 <span class="title">Users</span>
              </a>  
          </li>
          <li>
              <a href="/catalog/control/main?externalLoginKey=ELfccd14d0-0b7b-4ded-844d-d8a9b950187c">
              <#--  Product Catalog  -->
                 <span class="icon"><i style='font-size:24px' class='fas'>&#xf4ce;</i></span>
                 <span class="title">Product Catalog</span>
              </a>
          </li>
      </ul>


  </div>